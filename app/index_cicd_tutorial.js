var express = require('express');
var app = express();

app.get("/", function(req, res){
    res.status(200).send("Hello from Gitlab CI CD Tutorial");
});

app.listen(8080);
